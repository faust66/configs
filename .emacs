(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; DISABLE SHIT
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(fringe-mode 0)

(setq make-backup-files nil)
(setq auto-save-list-file-name nil)
(setq auto-save-default nil)

;; QUICK LISP
(load (expand-file-name "~/quicklisp/slime-helper.el"))
  ;; Replace "sbcl" with the path to your implementation
  (setq inferior-lisp-program "sbcl")

;; APPEARANCE
;; (require 'monokai-theme)
;; (load-theme 'monokai t)

(require 'zenburn-theme)
(load-theme 'manoj-dark t)
;; (set-background-color "#000000")
;; (set-background-color "#101010")
;;(set-background-color "#3f3f3f")
;; (set-cursor-color "red3")
;; (require 'soothe-theme)
;; (load-theme 'soothe 1)
(blink-cursor-mode 0)

(setq show-paren-style 'expression)
(show-paren-mode 2)

;; LINE NUMBERS
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

;; IDO MODE
(require 'ido)
(ido-mode 1)
(setq ido-enable-flex--matching t)

;; BUFFER LIST
(require 'bs)
(setq bs-configurations
      '(("files" "^\\*scratch\\*" nil nil bs-visits-non-file bs-sort-buffer-interns-are-last)))

;; AUTO COMPLETE
(require 'auto-complete-config)
(ac-config-default)
;; (ac-config-default)

;; SPEED BAR
(require 'sr-speedbar)
(global-set-key (kbd "<f11>") 'sr-speedbar-toggle)

;; RANGER
(require 'ranger)
(global-set-key (kbd "<f12>") 'ranger)

;; BUFFER MENU
(global-set-key (kbd "<f2>") 'bs-show)

;; EVIL MODE
(require 'evil)
(evil-mode 1)

;; LUA MODE
;;(require 'lua-mode)
;;(lua-mode 1)

(require 'yasnippet)
(yas-global-mode 1)


;; COMPILE C++
(defun code-compile ()
  (interactive)
  (unless (file-exists-p "Makefile")
    (set (make-local-variable 'compile-command)
     (let ((file (file-name-nondirectory buffer-file-name)))
       (format "%s -o %s %s"
           (if  (equal (file-name-extension file) "cpp") "g++" "gcc" )
           (file-name-sans-extension file)
           file)))
    (compile compile-command)))

(global-set-key [f9] 'code-compile)

;; (use-package auto-complete
;; (use-package flycheck
  ;; :ensure t
  ;; :init
  ;; (global-flycheck-mode t))

;; KEY BINDINGS
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "RET") 'newline)

(global-set-key (kbd "C-k") 'evil-buffer)
(global-set-key (kbd "C-K") 'evil-window-delete)

(global-set-key (kbd "C-;") 'comment-line)

;; WEIRD STUFF
;; (message "hello")

;; (defun awesome-func ()
  ;; (message "Hello idiot")
  ;; (evil-window-split))

;; (awesome-func)

;; (defun awesome-2 ()
  ;; (interactive)
  ;; (message "fuck you")
  ;; (evil-window-split))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(manoj-dark))
 '(custom-safe-themes
   '("fc48cc3bb3c90f7761adf65858921ba3aedba1b223755b5924398c666e78af8b" "78e6be576f4a526d212d5f9a8798e5706990216e9be10174e3f3b015b8662e27" "e7ba99d0f4c93b9c5ca0a3f795c155fa29361927cadb99cfce301caf96055dfd" "b0334e8e314ea69f745eabbb5c1817a173f5e9715493d63b592a8dc9c19a4de6" "c4cecd97a6b30d129971302fd8298c2ff56189db0a94570e7238bc95f9389cfb" default))
 '(package-selected-packages
   '(monokai-theme twilight-theme flycheck yasnippet-snippets yasnippet rust-mode lua-mode sr-speedbar ranger auto-complete slime zenburn-theme evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
