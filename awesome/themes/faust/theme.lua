local themes_path = require("gears.filesystem").get_themes_dir()
local dpi = require("beautiful.xresources").apply_dpi

local theme = {}
local theme_dir      = "~/.config/awesome/themes/faust/"
local icons_dir      = "~/.config/awesome/themes/faust/icons/"
local wallpapers_dir = theme_dir .. "wallpapers/"
local icons_layouts  = theme_dir .. "icons/layouts/"
local icons_taglist  = theme_dir .. "icons/taglist/"
local icons_titlebar = theme_dir .. "icons/titlebar/"
theme.wallpaper      = wallpapers_dir .. "girl.jpg"

theme.font = "Iosevka Extrabold 10"
--theme.font = "Cantarell Regular 10"

-- Colors
black = "#000000"
red   = "#c2190d"
green = "#60b48a"

zenburn_background = "#3f3f3f"
zenburn_background_dark = "#202020"
zenburn_green_light = "#bfebbf"
zenburn_green_dark = "#60b48a"

theme.fg_normal  = "#DCDCCC"
theme.fg_focus   = zenburn_green_dark
theme.fg_urgent  = red
theme.bg_normal  = black
theme.bg_focus   = black
theme.bg_urgent  = "#3F3F3F"
theme.bg_systray = theme.bg_normal

theme.useless_gap   = dpi(0)
theme.border_width  = dpi(1)
theme.border_width_floating  = dpi(8)
theme.border_normal = black
theme.border_focus  = zenburn_green_dark
theme.border_marked = "#CC9393"

theme.titlebar_bg_focus  = zenburn_background_dark
theme.titlebar_bg_normal = zenburn_background_dark

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#CC9393"

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]

-- Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = dpi(30)
theme.menu_width  = dpi(200)

theme.taglist_squares_sel   = icons_taglist .. "squarefz.png"
theme.taglist_squares_unsel = icons_taglist .. "squarez.png"
--theme.taglist_squares_resize = "true"

theme.awesome_icon          = icons_dir .. "awesome-icon.png"
--theme.menu_submenu_icon     = icons_dir .. "submenu.png"

theme.layout_tile       = icons_layouts .. "tile.png"
theme.layout_tileleft   = icons_layouts .. "tileleft.png"
theme.layout_tilebottom = icons_layouts .. "tilebottom.png"
theme.layout_tiletop    = icons_layouts .. "tiletop.png"
theme.layout_fairv      = icons_layouts .. "fairv.png"
theme.layout_fairh      = icons_layouts .. "fairh.png"
theme.layout_spiral     = icons_layouts .. "spiral.png"
theme.layout_dwindle    = icons_layouts .. "dwindle.png"
theme.layout_max        = icons_layouts .. "max.png"
theme.layout_fullscreen = icons_layouts .. "fullscreen.png"
theme.layout_magnifier  = icons_layouts .. "magnifier.png"
theme.layout_floating   = icons_layouts .. "floating.png"
theme.layout_cornernw   = icons_layouts .. "cornernw.png"
theme.layout_cornerne   = icons_layouts .. "cornerne.png"
theme.layout_cornersw   = icons_layouts .. "cornersw.png"
theme.layout_cornerse   = icons_layouts .. "cornerse.png"

theme.titlebar_close_button_focus  = icons_titlebar .. "close_focus.png"
theme.titlebar_close_button_normal = icons_titlebar .. "close_normal.png"

theme.titlebar_minimize_button_normal = icons_titlebar .. "minimize_normal.png"
theme.titlebar_minimize_button_focus  = icons_titlebar .. "minimize_focus.png"

theme.titlebar_ontop_button_focus_active        = icons_titlebar .. "ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = icons_titlebar .. "ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = icons_titlebar .. "ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = icons_titlebar .. "ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active       = icons_titlebar .. "sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = icons_titlebar .. "sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = icons_titlebar .. "sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = icons_titlebar .. "sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active     = icons_titlebar .. "floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = icons_titlebar .. "floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = icons_titlebar .. "floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = icons_titlebar .. "floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active    = icons_titlebar .. "maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = icons_titlebar .. "maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = icons_titlebar .. "maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = icons_titlebar .. "maximized_normal_inactive.png"

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=2:softtabstop=4:textwidth=80
